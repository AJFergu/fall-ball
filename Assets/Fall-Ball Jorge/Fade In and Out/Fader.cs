﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fader : MonoBehaviour {

	public string loadLevel;

	IEnumerator Begin()
    {
        yield return new WaitForSeconds(0.5f);        //Este es tiempo de espera para que cambie la escena
		SceneManager.LoadScene(loadLevel);            //Esta es la escena a la que quieres cambiar
        
    }
    public void LoadByIndex(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);         //Este es para cambiar escena, no tocar
    }
}
