﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    // NO TOCAR ALTERAR SALVO QUE SE INDIQUE

public class FedeOut : MonoBehaviour {
    public Texture2D fadeOutTexture;
    public float Speed = 1.25f;         //La velocidad con la que se cambia Se puede alterar
    
    private int Depth = -99;
    private float Alpha = 1.0f;
    private int fadeDirection = -1;

    void OnGUI()
    {
        Alpha = Alpha + fadeDirection * Speed * Time.deltaTime;
        Alpha = Mathf.Clamp01(Alpha);
        GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, Alpha);
        GUI.depth = Depth;
        GUI.DrawTexture(new Rect (0,0,Screen.width, Screen.height), fadeOutTexture);
    }
    
    public float BeginFade()
    {
        fadeDirection = 1;
        return (Speed);
    }
}
