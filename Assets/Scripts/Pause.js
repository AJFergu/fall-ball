﻿#pragma strict

	import UnityEngine.UI;

	private var pauseGame : boolean = false;
	private var showUI : boolean = false;
	var PauseImage : GameObject;

	function Start () 
	{
	//	GameMaster.currentCoins += 50;
	}

	function OpenPause ()
	{
		Time.timeScale = 0;
		showUI = true;
	}

	function ClosePause ()
	{
		showUI = false;
		Time.timeScale = 1;
	}

	function Update () 
	{
		pauseGame = !pauseGame;
		if(showUI == true)
		{
			PauseImage.SetActive(true);
		}
		else
		{
			PauseImage.SetActive(false);
		}
	}
