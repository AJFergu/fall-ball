﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoveButton : MonoBehaviour 
{
	Vector3 startPos;
	Vector3 endPos;
	Vector3 LeftLane;
	Vector3 RightLane;
	float perc = 1;
	void Start () 
	{
		LeftLane = new Vector3 (-200F,-100F,0F);
		RightLane = new Vector3 (200F,-100F,0F);
	}
	public void MoveLeft()
	{
		endPos = new Vector3 (transform.position.x - 100F, transform.position.y, transform.position.z);
		gameObject.transform.position = Vector3.Lerp (startPos, endPos, perc);
	}
	public void MoveRight()
	{
		endPos = new Vector3 (transform.position.x + 100F, transform.position.y, transform.position.z);
		gameObject.transform.position = Vector3.Lerp (startPos, endPos, perc);
	}
}