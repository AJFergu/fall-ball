﻿#pragma strict

	import UnityEngine.UI;

	public var Ball : GameObject;
	public var Canvas : Transform;
	private var Rand : int;
	private var Rand2 : int;
	private var SpawnRotationX = 0;
	private var SpawnRotationY = 180;
	private var SpawnRotationZ = 0;
	private var firstLaneX = -200;
	private var secondLaneX = -100;
	private var thirdLaneX = 0.0;
	private var fourthLaneX = 100;
	private var fifthLaneX = 200;
	private var allLaneY = 200;
	private var allLaneZ = 0.5;

	function Start () 
	{
		Rand = Random.Range(1,6);
		if(Rand == 1)
		{
			yield WaitForSeconds (0.2);	
		}
		if(Rand == 2)
		{
			yield WaitForSeconds (0.4);	
		}
		if(Rand == 3)
		{
			yield WaitForSeconds (0.6);	
		}
		if(Rand == 4)
		{
			yield WaitForSeconds (0.8);	
		}		
		if(Rand == 5)
		{
			yield WaitForSeconds (1.2);	
		}

		Randomizer();
	}

	public function Randomizer () 
	{
		Rand2 = Random.Range(1,6);
		if(Rand2 == 1)
		{
			Instantiate(Ball,Vector3 (firstLaneX, allLaneY, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(Canvas, false);	
		}
		if(Rand2 == 2)
		{
			Instantiate(Ball,Vector3 (secondLaneX, allLaneY, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(Canvas, false);	
		}
		if(Rand2 == 3)
		{
			Instantiate(Ball,Vector3 (thirdLaneX, allLaneY, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(Canvas, false);		
		}
		if(Rand2 == 4)
		{
			Instantiate(Ball,Vector3 (fourthLaneX, allLaneY, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(Canvas, false);		
		}
		if(Rand == 5)
		{
			Instantiate(Ball,Vector3 (fifthLaneX, allLaneY, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(Canvas, false);	
		}
		Start();
	}
