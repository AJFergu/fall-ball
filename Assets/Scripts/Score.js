﻿#pragma strict

	import UnityEngine.UI;
	var ball : Transform;
	var start : Transform;
	
	var scoreText : Text;

	function Start ()
	{
		GameMaster.currentScore = 0;
	}

	function Update ()
	{
		if (GameMaster.GM.GameRunning == true) 
		{
			GameMaster.currentScore = Vector3.Distance(ball.position, start.position) / 10;
		}
		scoreText.text = GameMaster.currentScore + "m"; 
	}