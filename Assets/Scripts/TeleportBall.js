﻿#pragma strict
	
	import UnityEngine.UI;
	private var LeftSide : Vector3;
	private var RightSide : Vector3;
	private var LeftLane : Vector3;
	private var RightLane : Vector3;
	var startPos : Vector3;
	var endPos : Vector3;
	public var perc : float = 1;
	
	function Start ()
	{
		
	}
	
	function OnTriggerEnter (info : Collider) 
	{
		if (info.tag == "Teleport Left Wall")
		{
			endPos = new Vector3 (transform.position.x + 500F, transform.position.y, transform.position.z);
			gameObject.transform.position = Vector3.Lerp (startPos, endPos, perc);
		}
		if (info.tag == "Teleport Right Wall")
		{
			endPos = new Vector3 (transform.position.x - 500F, transform.position.y, transform.position.z);
			gameObject.transform.position = Vector3.Lerp (startPos, endPos, perc);
		}
	}
