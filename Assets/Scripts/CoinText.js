﻿#pragma strict

	var CoinTextReal : Text;
	var CoinTextEffect : Text;
	static var CT : CoinText;

	function Start () 
	{
		CT = this;
		CoinTextReal.enabled = true;
		CoinTextEffect.enabled = false;
	}

	function Update () 
	{
		CoinTextReal.text = GameMaster.GM.currentCoins.ToString();
		CoinTextEffect.text = GameMaster.GM.effectCoins.ToString();
	}

	public function PreCoinEffect()
	{
		GameMaster.GM.effectCoins = GameMaster.GM.currentCoins;
		CoinTextReal.enabled = false;
		CoinTextEffect.enabled = true;
		GameMaster.GM.currentCoins += DeadScore.DS.coinsEarnedText;
	}
	public function CoinEffect()
	{
		yield WaitForSeconds(0.583333333333333333);
		GameMaster.GM.effectCoins += 1;
	}