#pragma strict

	import UnityEngine.UI;
	static var PP : PlayerPrefrences;

	function Start () 
	{
		PP = this;
		GameMaster.GM.currentCoins = PlayerPrefs.GetInt("Coins");
	}
	function Update () 
	{
			PlayerPrefs.SetInt("Coins", GameMaster.GM.currentCoins);
			PlayerPrefs.Save();
	}

	public function CheckHighScore()
	{
		if(GameMaster.currentScore > PlayerPrefs.GetInt("HighScore"))
		{
			HighScoreText.HST.StartHighScore();
			PlayerPrefs.SetInt("HighScore",GameMaster.currentScore);
			PlayerPrefs.Save();
		}
	}
