﻿#pragma strict

	import UnityEngine.UI;

	public var DeathSound : AudioSource;
	var DeathEffect : Transform;
	var StartPosition : Transform;
	
	function OnTriggerEnter (info : Collider) 
	{
		if (info.tag == "Enemy")
		{
			PlayerPrefrences.PP.CheckHighScore();
			DeadMenu1.DM1.DeadMenu();
			DeadMenu2.DM2.DeadMenu();
			DeadMenu3.DM3.DeadMenu();
			DeadMenu4.DM4.DeadMenu();
			DeadScore.DS.Dead = true;
			DeadScore.DS.PreMainDeadScore();
			CoinText.CT.PreCoinEffect();
			Instantiate(DeathEffect, transform.position, transform.rotation);
			DeathSound.Play();
			GameMaster.GameRunning = false;
			transform.position = StartPosition.position;
			//Destroy (gameObject, 10);
			//Debug.Log("DEATH!!!");
		}
	}
