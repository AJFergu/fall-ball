﻿#pragma strict

	static var LWBR : LuckyWheelBar;
	var NewImageLeft : Image;
	var NewImageRight : Image;
	var NewTextLeft : Text;
	var NewTextRight : Text;
	var Bar : Image;
	static var IsNew : boolean;

	function Start () 
	{
	//	IsNew = true;
		LWBR = this;
		Bar.enabled = false;
	}

	function Update () 
	{

	}

	function StartBar () 
	{
		yield WaitForSeconds (1);
		if(IsNew == true)
		{
			NewImageLeft.enabled = true;
			NewImageRight.enabled = true;
			NewTextLeft.enabled = true;
			NewTextRight.enabled = true;
			LuckyWheelParticleLeft.LWPL.ParticleStart();
			LuckyWheelParticleRight.LWPR.ParticleStart();
		}
		if(IsNew == false)
		{
			NewImageLeft.enabled = false;
			NewImageRight.enabled = false;
			NewTextLeft.enabled = false;
			NewTextRight.enabled = false;
		}
		yield WaitForSeconds (4);
		Bar.enabled = true;
		GetComponent.<Animator>().SetTrigger("StartBar");
	}