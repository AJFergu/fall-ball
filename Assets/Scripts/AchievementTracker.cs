﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class AchievementTracker : MonoBehaviour {

	void Start ()
	{
//		PlayerPrefs.SetString ("Achievement1", "Locked");
//		PlayerPrefs.SetString ("Achievement2", "Locked");
//		PlayerPrefs.SetString ("Achievement3", "Locked");
//		PlayerPrefs.SetString ("Achievement4", "Locked");
//		PlayerPrefs.SetString ("Achievement5", "Locked");
	}

	void Update () 
	{
		if(PlayerPrefs.GetString("Achievement1") == "Unlocked")
		{
			Social.ReportProgress (FallBallResources.achievement_unlock_all_balls, 100.0f, (bool sucess) => 
			{
					//				PlayerPrefs.SetInt ("Coins", 5000);
			});
		}
		if(PlayerPrefs.GetString ("Achievement2") == "Unlocked")
		{
			Social.ReportProgress (FallBallResources.achievement_reach_20000_meters, 100.0f, (bool sucess) => 
				{
					//				PlayerPrefs.SetInt ("Coins", 5000);
				});
		}
		if(PlayerPrefs.GetString ("Achievement3") == "Unlocked")
		{
			Social.ReportProgress (FallBallResources.achievement_die_500_times_from_spinning_spikes, 100.0f, (bool sucess) => 
				{
					//				PlayerPrefs.SetInt ("Coins", 5000);
				});
		}
		if(PlayerPrefs.GetString ("Achievement4") == "Unlocked")
		{
			Social.ReportProgress (FallBallResources.achievement_fall_1000000_meters_alltogether, 100.0f, (bool sucess) => 
				{
					//				PlayerPrefs.SetInt ("Coins", 5000);
				});
		}
		if(PlayerPrefs.GetString ("Achievement5") == "Unlocked")
		{
			Social.ReportProgress (FallBallResources.achievement_watch_100_advertisements, 100.0f, (bool sucess) => 
				{
					//				PlayerPrefs.SetInt ("Coins", 5000);
				});
		}
	}

	public void Achievement1 ()
	{
		Social.ReportProgress (FallBallResources.achievement_unlock_all_balls, 100.0f, (bool sucess) => 
		{
			PlayerPrefs.SetInt ("Coins", 5000);
		});
	}
}
