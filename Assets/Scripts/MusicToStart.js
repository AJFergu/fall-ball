﻿#pragma strict

	static var MTS : MusicToStart;
	public var StartMusicPrefab : Transform;

	function Start () 
	{
		MTS = this;
		yield WaitForSeconds (0.1);
		SMusicManager();
	}

	public function SMusicManager ()
	{
		var SMManager = Instantiate (StartMusicPrefab, transform.position, Quaternion.identity);
		SMManager.name = StartMusicPrefab.name;
		DontDestroyOnLoad (SMManager);
		yield WaitForSeconds(1);
		Destroy(gameObject);
	}