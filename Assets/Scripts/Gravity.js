﻿#pragma strict

import UnityEngine.UI;

	var countdownText : Text;

	function Start () 
	{
		GameMaster.GameRunning = false;
		countdownText.text = "3";
		yield WaitForSeconds (1);
		countdownText.text = "2";
		yield WaitForSeconds (1);
		countdownText.text = "1";
		yield WaitForSeconds (1);
		countdownText.text = "";
		GameMaster.GameRunning = true;
	}

	function Update () 
	{ 
		if (GameMaster.GM.GameRunning == true)
		{
   			GetComponent.<Rigidbody>().velocity = Vector3(0, -400, 0);
   		}
   		else
   		{
   			GetComponent.<Rigidbody>().velocity = Vector3(0, 0, 0);
   		}
	}