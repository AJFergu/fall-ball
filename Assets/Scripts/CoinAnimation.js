﻿#pragma strict

	public var Coin : Transform;
	public var ParentObject : Transform;
	public var TimesCoin : int;
	private var SpawnRotationX = 0;
	private var SpawnRotationY = 0;
	private var SpawnRotationZ = 0;
	private var allLaneX = 0;
	private var allLaneY = 0;
	private var allLaneZ = 0;
	if(GameMaster.GameRunning == false && DeadScore.DS.deadCurrentScore == 0 && TimesCoin != DeadScore.DS.coinsEarned)
	{
		TimesCoin += 1;
		Instantiate(Coin,Vector3 (allLaneX, allLaneY, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(ParentObject, false);
	}