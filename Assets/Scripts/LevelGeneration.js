﻿#pragma strict
	
	import UnityEngine.UI;

	static var LG : LevelGeneration;
	public var x1Walls0 : Transform;
	public var x1Walls1 : Transform;
	public var x1Walls2 : Transform;
	public var x1Walls3 : Transform;
	public var x1Walls4 : Transform;
	public var x1Walls5 : Transform;
	public var x1Walls6 : Transform;
	public var x1Walls7 : Transform;
	public var x1Walls8 : Transform;
	public var x1Walls9 : Transform;
	public var x2Walls0 : Transform;
	public var x2Walls1 : Transform;
	public var x2Walls2 : Transform;
	public var x2Walls3 : Transform;
	public var x2Walls4 : Transform;
	public var x2Walls5 : Transform;
	public var x2Walls6 : Transform;
	public var x2Walls7 : Transform;
	public var x2Walls8 : Transform;
	public var x2Walls9 : Transform;
	public var x3Walls0 : Transform;
	public var x3Walls1 : Transform;
	public var x3Walls2 : Transform;
	public var x3Walls3 : Transform;
	public var x3Walls4 : Transform;
	public var x3Walls5 : Transform;
	public var x3Walls6 : Transform;
	public var x3Walls7 : Transform;
	public var x3Walls8 : Transform;
	public var x3Walls9 : Transform;
	public var x0Walls0 : Transform;
	public var x0Walls1 : Transform;
	public var x0Walls2 : Transform;
	public var x0Walls3 : Transform;
	public var x0Walls4 : Transform;
	public var x0Walls5 : Transform;
	public var x0Walls6 : Transform;
	public var x0Walls7 : Transform;
	public var x0Walls8 : Transform;
	public var x0Walls9 : Transform;

	private var FirstRand : int;
	private var RandWalls1x : int;
	private var RandWalls2x : int;
	private var RandWalls3x : int;
	private var RandWalls0x : int;
	private var disPlayer : int = 49500;
	private var SpawnRotationX = 0;
	private var SpawnRotationY = 0;
	private var SpawnRotationZ = 0;
	private var allLaneX = 0;
	private var allLaneY = 0;
	private var allLaneZ = 0;
	public var CanvasWorldSpace : Transform;

	function Start ()
	{
		LG = this;
		Break();
	}

	public function Break ()
	{
		FirstRand = Random.Range(1,6);
		if(FirstRand == 1)
		{
			Walls1x();	
		}
		if(FirstRand == 2 || FirstRand == 3)
		{
			Walls2x();	
		}
		if(FirstRand == 4)
		{
			Walls3x();	
		}
		if(FirstRand == 5)
		{
			Walls0x();	
		}
	}
	
	public function Walls1x ()
	{
		RandWalls1x = Random.Range(1,11);
		if(RandWalls1x == 1)
		{
			disPlayer -= 1000;
			Instantiate(x1Walls0,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls1x == 2)
		{
			disPlayer -= 1000;
			Instantiate(x1Walls1,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls1x == 3)
		{
			disPlayer -= 1000;
			Instantiate(x1Walls2,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls1x == 4)
		{
			disPlayer -= 1000;
			Instantiate(x1Walls3,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls1x == 5)
		{
			disPlayer -= 1000;
			Instantiate(x1Walls4,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls1x == 6)
		{
			disPlayer -= 1000;
			Instantiate(x1Walls5,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls1x == 7)
		{
			disPlayer -= 1000;
			Instantiate(x1Walls6,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls1x == 8)
		{
			disPlayer -= 1000;
			Instantiate(x1Walls7,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls1x == 9)
		{
			disPlayer -= 1000;
			Instantiate(x1Walls8,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls1x == 10)
		{
			disPlayer -= 1000;
			Instantiate(x1Walls9,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		yield WaitForSeconds (1);
		Break();
	}
	
	public function Walls2x ()
	{
		RandWalls2x = Random.Range(1,11);
		if(RandWalls2x == 1)
		{
			disPlayer -= 1000;
			Instantiate(x2Walls0,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls2x == 2)
		{
			disPlayer -= 1000;
			Instantiate(x2Walls1,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls2x == 3)
		{
			disPlayer -= 1000;
			Instantiate(x2Walls2,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls2x == 4)
		{
			disPlayer -= 1000;
			Instantiate(x2Walls3,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls2x == 5)
		{
			disPlayer -= 1000;
			Instantiate(x2Walls4,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls2x == 6)
		{
			disPlayer -= 1000;
			Instantiate(x2Walls5,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls2x == 7)
		{
			disPlayer -= 1000;
			Instantiate(x2Walls6,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls2x == 8)
		{
			disPlayer -= 1000;
			Instantiate(x2Walls7,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls2x == 9)
		{
			disPlayer -= 1000;
			Instantiate(x2Walls8,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls2x == 10)
		{
			disPlayer -= 1000;
			Instantiate(x2Walls9,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		yield WaitForSeconds (1);
		Break();
	}
	
	public function Walls3x ()
	{
		RandWalls3x = Random.Range(1,11);
		if(RandWalls3x == 1)
		{
			disPlayer -= 1000;
			Instantiate(x3Walls0,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls3x == 2)
		{
			disPlayer -= 1000;
			Instantiate(x3Walls1,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls3x == 3)
		{
			disPlayer -= 1000;
			Instantiate(x3Walls2,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls3x == 4)
		{
			disPlayer -= 1000;
			Instantiate(x3Walls3,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls3x == 5)
		{
			disPlayer -= 1000;
			Instantiate(x3Walls4,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls3x == 6)
		{
			disPlayer -= 1000;
			Instantiate(x3Walls5,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls3x == 7)
		{
			disPlayer -= 1000;
			Instantiate(x3Walls6,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls3x == 8)
		{
			disPlayer -= 1000;
			Instantiate(x3Walls7,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls3x == 9)
		{
			disPlayer -= 1000;
			Instantiate(x3Walls8,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls3x == 10)
		{
			disPlayer -= 1000;
			Instantiate(x3Walls9,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		yield WaitForSeconds (1);
		Break();
	}
	
	public function Walls0x ()
	{
		RandWalls0x = Random.Range(1,11);
		if(RandWalls0x == 1)
		{
			disPlayer -= 1000;
			Instantiate(x0Walls0,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls0x == 2)
		{
			disPlayer -= 1000;
			Instantiate(x0Walls1,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls0x == 3)
		{
			disPlayer -= 1000;
			Instantiate(x0Walls2,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls0x == 4)
		{
			disPlayer -= 1000;
			Instantiate(x0Walls3,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls0x == 5)
		{
			disPlayer -= 1000;
			Instantiate(x0Walls4,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls0x == 6)
		{
			disPlayer -= 1000;
			Instantiate(x0Walls5,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls0x == 7)
		{
			disPlayer -= 1000;
			Instantiate(x0Walls6,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls0x == 8)
		{
			disPlayer -= 1000;
			Instantiate(x0Walls7,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls0x == 9)
		{
			disPlayer -= 1000;
			Instantiate(x0Walls8,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		if(RandWalls0x == 10)
		{
			disPlayer -= 1000;
			Instantiate(x0Walls9,Vector3 (allLaneX, disPlayer, allLaneZ), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(CanvasWorldSpace, false);
		}
		yield WaitForSeconds (1);
		Break();
	}
	
		
		
		
		