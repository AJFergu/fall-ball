﻿#pragma strict

	static var DM1 : DeadMenu1;

	function Start () 
	{
		DM1 = this;
	}

	function Update () 
	{

	}

	public function DeadMenu ()
	{
		yield WaitForSeconds (0.25);
		GetComponent.<Animator>().SetTrigger("Start Death");
	}

	public function EndDeadMenu ()
	{
		HighScoreText.HST.CloseHighScore();
//		yield WaitForSeconds (0.25);
		GetComponent.<Animator>().SetTrigger("End Death");
	}