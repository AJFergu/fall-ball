﻿#pragma strict

	public var Coin : Transform;
	public var ParentObject : Transform;
	public var TimesCoin : int;
	public var StartCoinCycle : boolean = true;
	private var SpawnRotationX = 0;
	private var SpawnRotationY = 0;
	private var SpawnRotationZ = 0;
	private var allLaneX = 115;
	private var allLaneY = 5;
	private var allLaneZ = 0;
	var coinsEarned : float;
	var coinsEarnedTest : int;
	var coinsEarnedText : int;
	var scoreText : Text;
	var deadCurrentScore : int;
	static var DS : DeadScore;
	var Dead : boolean;
	
	function Start () 
	{
		DS = this;
		TimesCoin = 0;
		coinsEarned = 0;
		Dead = false;
	}

	function Update () 
	{
		coinsEarnedTest = Mathf.Floor(coinsEarned);
		scoreText.text = deadCurrentScore + "m = " + Mathf.Floor(coinsEarned).ToString();
		if(deadCurrentScore == 0 && StartCoinCycle == true && Dead == true)
		{
			StartCoinCycle = false;
			CoinBreak ();
		}
	}
	public function PreMainDeadScore ()
	{
		coinsEarnedText = Mathf.Floor(GameMaster.currentScore) / 100;
		deadCurrentScore = GameMaster.currentScore;
		yield WaitForSeconds(1.9);
		Break();
	}
	public function Break ()
	{
		yield WaitForSeconds(0.1);
		MainDeadScore();
	}

	public function MainDeadScore ()
	{
		if (GameMaster.GameRunning == false)
		{
			if (deadCurrentScore > 0)
			{
				deadCurrentScore -= 1;
				coinsEarned += 0.01;
			}
			if (deadCurrentScore > 10)
			{
				deadCurrentScore -= 10;
				coinsEarned += 0.1;
			}
			if (deadCurrentScore > 100)
			{
				deadCurrentScore -= 100;
				coinsEarned += 1;
			}
		}
		AfterMainDeadScore();
	}
	public function AfterMainDeadScore ()
	{
		Break();
	}

	public function CoinBreak ()
	{
		yield WaitForSeconds (0.3);
		CoinAnimation ();
	}
	public function CoinAnimation () 
	{
		if(GameMaster.GameRunning == false && deadCurrentScore == 0 && TimesCoin != coinsEarnedTest)
		{
			TimesCoin += 1;
			Instantiate(Coin,gameObject.transform.position, Quaternion.identity).transform.SetParent(ParentObject, false);
			CoinText.CT.CoinEffect();
		}
		CoinBreak ();
	}