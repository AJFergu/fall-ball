﻿#pragma strict

	import UnityEngine.UI;

	static var LWPL : LuckyWheelParticleLeft;
	public var ParticleEffect : Transform;
	public var ParticleEffectGameObjectLeft : Transform;
	private var SpawnRotationX = -90;
	private var SpawnRotationY = 0;
	private var SpawnRotationZ = 0;

	function Start () 
	{
		LWPL = this;
	}

	function Update () 
	{

	}

	function ParticleStart ()
	{
		yield WaitForSeconds (5.5);
		Instantiate(ParticleEffect, Vector3 (transform.position.x, transform.position.y, transform.position.z), Quaternion.Euler(Vector3 (SpawnRotationX, SpawnRotationY, SpawnRotationZ))).transform.SetParent(ParticleEffectGameObjectLeft);
	}
