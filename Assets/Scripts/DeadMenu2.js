﻿#pragma strict

	static var DM2 : DeadMenu2;
	public var LuckyWheelButton : GameObject;
	public var VideoButton : GameObject;

	function Start () 
	{
		//GameMaster.GM.currentCoins += 55;
		DM2 = this;
		VideoButton.SetActive(true);
		LuckyWheelButton.SetActive(false);
	}

	function Update () 
	{
		
	}

	public function DeadMenu ()
	{
		yield WaitForSeconds (0.5);
		if(GameMaster.GM.currentCoins >= 10)
		{
			DeadMenu2ButtonSetLuckyWheelActive();
		}
		GetComponent.<Animator>().SetTrigger("Start Death");
	}

	public function EndDeadMenu ()
	{
//		yield WaitForSeconds (0.5);
		GetComponent.<Animator>().SetTrigger("End Death");
	}

	public function DeadMenu2ButtonSetLuckyWheelActive ()
	{
		VideoButton.SetActive(false);
		LuckyWheelButton.SetActive(true);
	}