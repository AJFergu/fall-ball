﻿#pragma strict

	static var MM : MusicManager;
	var PhysicalScienceStart : AudioClip;
	var PhysicalScienceMain : AudioClip;
	var StartAudioSource : AudioSource;
	var MainAudioSource : AudioSource;
	public var MainMusicPrefab : Transform;

	function Start () 
	{
		MM = this;
		yield WaitForSeconds (18.61);
		MMusicManager();
	}

	public function MMusicManager ()
	{
		var MMManager = Instantiate (MainMusicPrefab, transform.position, Quaternion.identity);
		MMManager.name = MainMusicPrefab.name;
		DontDestroyOnLoad (MMManager);
		yield WaitForSeconds(1);
		Destroy(gameObject);
	}

